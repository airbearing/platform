# IDR UPM Air Bearing

Onboard platform library for the Air Bearing. Developed for IDR - UPM (2019).

Library that is used to operate the onboard
platform subsystem, comprised by a Teensy 3.5 microcontroller, an Adafruit BNO055 inertial measurement unit and an Adafruit
RFM69HCW transceiver.

## Contents

- [Usage](#usage)
- [Dependencies](#dependencies)
- [Docs](#docs)
- [Authors](#authors)
- [References](#references)

## Usage

1. Install [Arduino IDE](https://www.arduino.cc/en/Main/Software)
2. Clone or download this `Platform` repository into the [default directory for the Arduino libraries](https://www.arduino.cc/en/guide/libraries) `~/Documents/Arduino/libraries/`.
3. Install [Teensyduino](https://www.pjrc.com/teensy/tutorial.html), the Teensy board manager for the Arduino IDE.
4. Install external libraries. [Check how](https://www.arduino.cc/en/guide/libraries).
5. Compile example into Teensy

## Dependencies

### Libraries installed with Arduino IDE

- [**SPI.h**](https://www.arduino.cc/en/reference/SPI): SPI coms between Teensy and RFM69HCW

### External libraries

- [**RH_RF69.h**](https://goo.gl/ZeEy5k): Handles transceiver general communications
- [**RHReliableDatagram.h**](https://goo.gl/Ks9rfE): Handles transceiver encrypted packet communications
- [**i2c_t3.h**](https://goo.gl/RyzFaM): In charge of I2C for Teensy 3.x
- [**Adafruit_Sensor.h**](https://github.com/adafruit/Adafruit_Sensor): Handles all types of Adafruit sensors
- [**Adafruit_BNO055_t3.h**](https://goo.gl/eAYKjn): Specific library for the Teensy 3.x and the BNO055 IMU

## Documentation

- Docs have been generated using  [Doxygen](http://www.doxygen.nl/manual/starting.html).
- Configuration is defined in `Doxyfile`.

### Generating or editting the documentation

1. Install  [Doxygen](http://www.doxygen.nl/manual/starting.html).
2. Install [Graphviz](https://www.graphviz.org/download/).
3. Go to Windows [environment variables](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/) and open the `PATH` variable.
4. Add Graphviz's `dot.exe` absolute path (something like `C:\Program Files (x86)\Graphviz2.38\bin`).
5. Modify comment blocks in code as desired.
6. Edit `Doxyfile` configuration file if desired.
7. Run:

```bash
doxygen Doxyfile
```

## Repository structure

|      File      |                                      Description                                      |
| -------------- | ------------------------------------------------------------------------------------- |
| `platform.h`   | Defines Platform class, macros, and libraries                                         |
| `platform.cpp` | Contains Platform class public and private methods                                    |
| `.gitignore`   | Tells Git to [ignore local files](https://git-scm.com/docs/gitignore)                 |
| `keywords.txt` | Tells Arduino to [highlight Platform class and main methods](https://goo.gl/F4Xsrk)   |
| `Doxyfile`     | Contains [Doxygen](http://www.doxygen.nl/manual/starting.html) documentation settings |

|          Folder          |                        Description                        |
| ------------------------ | --------------------------------------------------------- |
| :file_folder: `docs`     | Contains resources for Doxygen to generate documentation  |
| :file_folder: `examples` | Arduino sketches to be compiled on the Platform subsystem |

## Authors

- David Criado Pernía

## References

- [**Teensy Specs**](https://www.sparkfun.com/products/14055)
- [**Teensy Pinout**](https://www.pjrc.com/teensy/card8a_rev2.pdf)
- [Adafruit **RFM69HCW Specs**](https://www.adafruit.com/product/3071)
- [Adafruit **RFM69HCW Installation Tutorial**](https://learn.adafruit.com/adafruit-rfm69hcw-and-rfm96-rfm95-rfm98-lora-packet-padio-breakouts)
- [**RFM69HCW Datasheet**](https://fccid.io/ANATEL/01365-15-05508/Manual-para-referencia-RFM69HCW/BFBB2D4A-360A-44F8-B840-F3CF0B535FE6/PDF)
- [Adafruit **BNO055 Installation Tutorial**](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/overview)
- [**BNO055 Datasheet**](https://cdn-learn.adafruit.com/assets/assets/000/036/832/original/BST_BNO055_DS000_14.pdf)
