/*  Platform library for the Air Bearing.
 *  Developed for IDR - UPM (2019).
 *  Author: David Criado Pernia
 * 
 *  Library that is used to operate the onboard
 *  platform subsystem, comprised by a Teensy 
 *  3.5 microcontroller, an Adafruit BNO055 
 *  inertial measurement unit and an Adafruit
 *  RFM69HCW transceiver.
 *  File contains:
 *  - Begin methods
 *  - Telecommand methods
 *  - Operate methods
 *  - Telemetry methods
 *  - Miscelaneous and auxiliary methods
 */

#include "platform.h"

/**************************************************************************
 *  Inherited constructors
 *  Solved thanks to: https://goo.gl/H4yL13
 **************************************************************************/

// Instance of the IMU driver
Adafruit_BNO055 bno(WIRE_BUS, -1, BNO055_ADDRESS_A, I2C_MASTER,
                    I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_100, 
                    I2C_OP_MODE_ISR);
// Singleton instance of the radio driver
RH_RF69 rf69(RFM69_CS, RFM69_INT);
// Class to manage message delivery and receipt, using the driver above
RHReliableDatagram rf69_manager(rf69, PLATFORM_ADDRESS);

/**************************************************************************
 *  CLASS
 **************************************************************************/

// Platform class constructor
Platform::Platform(bool HAS_SERIAL, bool HAS_RADIO, bool HAS_IMU)
{
    // Alert if Platform cannot communicate anything outwards
    if (!HAS_SERIAL && !HAS_RADIO) alert();
    _HAS_SERIAL = HAS_SERIAL;
    _HAS_RADIO  = HAS_RADIO;
    _HAS_IMU    = HAS_IMU;
}

/**************************************************************************
 *  BEGIN functions
 **************************************************************************/

// Begin Teensy, Radio, IMU and Serial (only if applies)
void Platform::begin()
{
    // Start USB Serial connection if Receiver is set to have one
    if (_HAS_SERIAL) beginSerial();

    // Start Teensy
    beginTeensy();

    // Start Radio
    if (_HAS_RADIO && !beginRadio())
        alert("[ERROR]\tRadio cannot boot. Platform init stopped", 1);

    // Start IMU
    if (_HAS_IMU)
    {
        int8_t _imu_result = beginIMU();

        // IMU uncalibrated
        if (_imu_result == 0) return;

        // IMU did not boot
        if (_imu_result == -1)
            alert("[ERROR]\tIMU cannot boot. Platform init stopped", 3);
    }

    transmit("Platform booted successfully");

    // Set platform operational mode
    setOperationalMode(PLATFORM_OP_IDLE); // 00, 10, 20, 30
}

// Begin Serial port
void Platform::beginSerial()
{
    Serial.begin(BAUDRATE ? BAUDRATE : 115200);
    _SERIAL_BOOTED = true;
    delay(1000);
    transmit("Booting platform...");
    transmit("[1/4]\tSerial inited");
}

// Begin Teensy: init pins, led and SPI
void Platform::beginTeensy()
{
    // Init RST pins
    if (_HAS_RADIO) pinMode(RFM69_RST, OUTPUT);
    if (_HAS_IMU)   pinMode(BNO_RST,   OUTPUT);

    // Set RST pins to default
    // More info: https://goo.gl/HSzX36
    if (_HAS_RADIO) digitalWrite(RFM69_RST, LOW);
    if (_HAS_IMU)   digitalWrite(BNO_RST,   HIGH);

    // Init SPI
    if (_HAS_RADIO)
    {
        SPI.begin();
        SPI.setSCK(RFM69_SCK);
        SPI.setMOSI(RFM69_MOSI);
        SPI.setMISO(RFM69_MISO);
    }

    // Init LED after SPI to prevent pin 13 from being remapped
    // More info: https://goo.gl/Qinq1F
    pinMode(LED, OUTPUT);

    transmit("[2/4]\tTeensy successfully configured.");
}

// Begin radio transceiver
bool Platform::beginRadio()
{
    // Manual reset
    blink(RFM69_RST, 1, false);

    // Start sensor
    if (!rf69_manager.init()) return false;
    transmit("\tInited RFM69 radio. Setting up radio...");

    // Defaults 434MHz, GFSK_Rb250Fd250, +13dbM
    if (!rf69.setFrequency(RF69_FREQ)) return false;

    // Range from 14-20 for power, 2nd arg must be true for 69HCW
    rf69.setTxPower(20, true);

    // Key same as Station radio
    uint8_t key[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                     0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}; 
    rf69.setEncryptionKey(key);
    
    _RADIO_BOOTED = true;
    transmit("[3/4]\tRadio configured and ready");
    return true;
}

// Begin IMU
int8_t Platform::beginIMU()
{
    // Manual reset
    blink(BNO_RST, 1, true);

    // Start sensor
    delay(10);
    if (!bno.begin()) return -1;
    transmit("\tInited BNO055 IMU. Setting up IMU...");
    delay(1000);

    // Set offsets, clock and operation mode
    bno.setExtCrystalUse(true);
    bno.setMode(bno.OPERATION_MODE_NDOF); // Set IMU's op mode
    _IMU_BOOTED = true;
    if (!setCalibration())
    {
        transmit("Entering CALIB mode...");
        setOperationalMode(PLATFORM_OP_CALIB); // go to CALIB mode
        return 0;
    }
    transmit("[4/4]\tIMU calibrated and ready");
    return 1;
}

/**************************************************************************
 *  2. TELECOMMANDS
 **************************************************************************/

// 2.1 LISTEN
// =====================================================================

// Listen to telecommand via Serial or Radio
void Platform::listen()
{
    if (_HAS_SERIAL && listenSerial()) interpretCommand();
    if (_HAS_RADIO  && listenRadio())  interpretCommand();
}

// Listen if there is any telecommand via Serial
bool Platform::listenSerial()
{
    // If there's any serial available, read it
    while (Serial.available() > 0)
    {
        // Get command head, body and options
        uint8_t head = Serial.parseInt();
        uint8_t body = Serial.parseInt();
        uint8_t opts = Serial.parseInt();

        // Store received telecommand
        if (storeCommand(&head, &body, &opts))
            return true;
        return false;
    }
    return false;
}

// Listen if there is any telecommand via Radio
bool Platform::listenRadio()
{
    // Tests whether a new message is available from the Driver
    if (!rf69_manager.available())
        return false;

    uint8_t len = sizeof(_radio_buffer);
    uint8_t from;

    // Copy message to buffer and send ACK to Station
    if (!rf69_manager.recvfromAckTimeout(_radio_buffer, &len, 100, &from))
        return false;        

    // Zero out remaining string
    _radio_buffer[len] = 0;

    // Prepare incoming string
    uint8_t head, body, opts;
    if (!splitCommand(_radio_buffer, &head, &body, &opts))
        return false;

    // Store received telecommand
    if (storeCommand(&head, &body, &opts))
        return true;
    return false;
}

// Split incoming string "head,body,opts" into command parameters
bool Platform::splitCommand(uint8_t* buf, uint8_t* head, uint8_t* body, uint8_t* opts)
{
    String buf_str = String((char *)buf);
    if (!string2vector(&buf_str, head, body, opts))
        return false;
    return true;
}

// Store received telecommand
bool Platform::storeCommand(uint8_t* head, uint8_t* body, uint8_t* opts)
{
    // Skip if TC is "0,0,0"
    if (*head + *body + *opts <= 0) 
        return false;

    // Store telecommand received
    _cmd_head = *head;
    _cmd_body = *body;
    _cmd_opts = *opts;
    return true;
}

// 2.2 INTERPRET
// =====================================================================

void Platform::interpretCommand()
{
    // Set Operation Mode
    /////////////////////////////////
    // ADD HERE NEW OPMODE CONSTANTS
    if (_cmd_head == PLATFORM_OP_IDLE || _cmd_head == PLATFORM_OP_STATUS 
        ||_cmd_head == PLATFORM_OP_DATA || _cmd_head == PLATFORM_OP_CALIB)
    {
        interpretOpMode();
    }
    else if (_cmd_head == PLATFORM_CMD_INTERACTION)
    {
        if (_cmd_question_active) interpretInteraction();
    }
    else
    {
        transmit("[WARN]\tTelecommand head not valid");
        return;
    }
}

// Handle commands that change operational mode
void Platform::interpretOpMode()
{
    uint8_t _mode = _cmd_head;

    // Prevent opmodes DATA and CALIB if there is no IMU
    if (!_HAS_IMU && (_mode == PLATFORM_OP_DATA || _mode == PLATFORM_OP_CALIB))
    {
        transmit("[WARN]\tIMU required. Keeping previous OpMode");
        return;
    }

    // Notify operational modes that a new command has arrived!
    _new_command = true;

    // Reset interaction process
    resetInteraction();

    // Compare with stored operational mode
    if (_mode == PLATFORM_OPERATION_MODE) return;

    // If mode differs from stored, handle change
    transmit("!!\tOperational mode has changed");
    _counter = 0;

    // Handle change of Operational Mode
    if (_mode == PLATFORM_OP_IDLE)
        transmit("IDLE Mode");
    if (_mode == PLATFORM_OP_STATUS)
        transmit("STATUS Mode");
    if (_mode == PLATFORM_OP_DATA)
        transmit("DATA Mode");
    if (_mode == PLATFORM_OP_CALIB)
        transmit("CALIB Mode");
    /////////////////////////////////// ADD HERE NEW OP MODES
    delay(2000);

    // Store new mode
    PLATFORM_OPERATION_MODE = _mode;
}

// Handle interactive answers from the Station
void Platform::interpretInteraction()
{
    if (_cmd_body == PLATFORM_CMD_YES || _cmd_body == PLATFORM_CMD_NO)
        _cmd_interaction_response = _cmd_body;
}

// Reset interaction flags
void Platform::resetInteraction()
{
    _cmd_question_active = false;
    _cmd_interaction_response = 0;
}

/**************************************************************************
 *  3. OPERATE
 **************************************************************************/

// Operate according to PLATFORM_OPERATION_MODE
void Platform::operate()
{
    switch (PLATFORM_OPERATION_MODE)
    {
        case PLATFORM_OP_IDLE:      operateIdle();
            break;
        case PLATFORM_OP_STATUS:    operateStatus();
            break;
        case PLATFORM_OP_DATA:      operateData();
            break;
        case PLATFORM_OP_CALIB:     operateCalib();
            break;
        /////////////////////////////////// ADD HERE NEW OP MODES
    }

    // TODO: Add this functionality
    // selfCheck();
    ///////////////////////////////////
}

// IDLE operating mode
void Platform::operateIdle()
{
    // Perodic reminder
    if (_counter >= PLATFORM_IDLE_TIMEOUT * PLATFORM_OP_RATE)
    {
        transmit("Still on IDLE Mode. Awaiting for new telecommand...");
        blink(LED, 100);
        _counter = 0;
    }

    // Increment counter and delay
    _counter = _counter + 1;
    delay(long(1000 / PLATFORM_OP_RATE));
}

// STATUS operating mode
void Platform::operateStatus()
{
    if (_new_command)
    {
        // Set timeout between reports and format
        setStatusTimeout();
        setStatusFormat();

        // Okay, system have read the notification, now shut up
        _new_command = false;
    }
    
    // Get platform status with a given frequency (seconds)
    if (_counter == 0 || _counter >= _status_timeout * PLATFORM_OP_RATE)
    {
        reportStatus(); //////////////////////// CORE FUNCTION
        blink();
        _counter = 0;
    }

    // Increment counter and delay
    _counter = _counter + 1;
    delay(long(1000 / PLATFORM_OP_RATE));
}

// DATA operating mode
void Platform::operateData()
{
    if (_new_command)
    {
        // Set data type
        setDataType();

        // Okay, system have read the notification, now shut up
        _new_command = false;
    }

    // Get IMU data with a given frequency (seconds)
    if (_counter == 0 || _counter >= PLATFORM_OP_RATE)
    {
        reportData(); //////////////////////// CORE FUNCTION
        _counter = 0;
    }

    // Increment counter and delay
    _counter = _counter + 1;
    delay(10);
}

// CALIB operating mode
void Platform::operateCalib()
{
    // Wait
    delay(long(1000 / PLATFORM_OP_RATE));

    // IMU is not calibrated
    if (!bno.isFullyCalibrated())
    {
        // Question has not been asked yet
        if (!_cmd_question_active)
        {
            transmit("IMU is not calibrated");
            transmit("Do you want to enter manual calibration? [Y/N]");
            transmit("Yes:\t60,1,0");
            transmit("No:\t60,2,0");
            // Activate question-asked flag
            _cmd_question_active = true;
            return;
        }
        // Question has been asked, now handle response
        switch (_cmd_interaction_response)
        {
            case 0:
                break;
            case PLATFORM_CMD_YES:
            {
                reportIMUCalib(&_imu_c_sys, &_imu_c_acc, &_imu_c_gyr, &_imu_c_mag);
                verbalizeIMUCalib(&_imu_c_str, &_imu_c_sys, &_imu_c_acc, &_imu_c_gyr, &_imu_c_mag);
                transmit(_imu_c_str);
                break;
            }
            case PLATFORM_CMD_NO:
                resetInteraction();
                setOperationalMode(PLATFORM_OP_IDLE);
                break;
        }
    }
    // IMU is calibrated. Exit from Calib mode to Idle
    else
    {
        transmit("IMU is calibrated");
        transmit("Stored offsets are:");
        reportCalibOffsets();
        resetInteraction();
        setOperationalMode(PLATFORM_OP_IDLE);
    }    
}

/////////////////////////////////// ADD HERE NEW OP MODES

// Set operational mode
void Platform::setOperationalMode(uint8_t op_mode)
{
    delay(1000);
    transmit("Setting Operational Mode...");
    if (!op_mode)
    {
        op_mode = PLATFORM_OP_IDLE;
        transmit("Set to default mode: IDLE"); // assumes IDLE as default
    }

    // Assign as if it were a received telecommand
    _cmd_head = op_mode;

    // Trigger operational mode
    interpretCommand();
}

/**************************************************************************
 *  4. TELEMETRY
 **************************************************************************/

// 4.1 STATUS
// =====================================================================

// Get single report of status
void Platform::reportStatus()
{
    // Update epoch string
    time2string(&_epoch_str);

    // Format and report
    if (_status_report_format == 0)
    {
        transmit("-----------REPORT-----------");
        transmit(_epoch_str);
        if (_HAS_RADIO)
        {
            reportRadioStatus(&_radio_rssi);
            verbalizeRadioStatus(&_rad_str, &_radio_rssi);
            transmit(_rad_str);
        }
        if (_HAS_IMU)
        {
            reportIMUCalib (&_imu_c_sys, &_imu_c_acc, &_imu_c_gyr, &_imu_c_mag);
            reportIMUHealth(&_imu_h_status, &_imu_h_res, &_imu_h_err, &_imu_h_temp);
            verbalizeIMUCalib (&_imu_c_str, &_imu_c_sys, &_imu_c_acc, &_imu_c_gyr, &_imu_c_mag);
            verbalizeIMUHealth(&_imu_h_str, &_imu_h_status, &_imu_h_res, &_imu_h_err, &_imu_h_temp);
            transmit(_imu_c_str);
            transmit(_imu_h_str);
        }
    }
    // Join values in CSV format
    else
    {
        String _imuC, _imuH, _row_str;
        if (_HAS_RADIO)
            reportRadioStatus(&_radio_rssi);
        if (_HAS_IMU)
        {
            reportIMUCalib(&_imu_c_sys, &_imu_c_acc, &_imu_c_gyr, &_imu_c_mag);
            reportIMUHealth(&_imu_h_status, &_imu_h_res, &_imu_h_err, &_imu_h_temp);
            int _imu_c[4] = {_imu_c_sys, _imu_c_acc, _imu_c_gyr, _imu_c_mag};
            int _imu_h[4] = {_imu_h_status, _imu_h_res, _imu_h_err, _imu_h_temp};
            join(&_imuC, _imu_c);
            join(&_imuH, _imu_h);
        }
        String row[4] = {_epoch_str, String(_radio_rssi), _imuC, _imuH};
        join(&_row_str, row);
        transmit(_row_str);
    }
}

// Compose verbose status of radio parameters
void Platform::verbalizeRadioStatus(String *message, int8_t *rssi)
{
    *message = "[RAD]\tAddress: ";    message->concat((int)PLATFORM_ADDRESS);
    message->concat(", RSSI: ");  message->concat(*rssi);
    message->concat(" dBm");
}

// Compose verbose status of IMU calibration
void Platform::verbalizeIMUCalib(String *message, uint8_t *_system, uint8_t *acc, uint8_t *gyr, uint8_t *mag)
{
    *message = "[CAL]\tSystem: ";  message->concat(*_system);
    message->concat(", Acc: ");    message->concat(*acc);
    message->concat(", Gyr: ");    message->concat(*gyr);
    message->concat(", Mag: ");    message->concat(*mag);
}

// Compose verbose status of IMU BNO055 registers
void Platform::verbalizeIMUHealth(String *message, uint8_t *status, uint8_t *result, uint8_t *error, int8_t *temp)
{
    *message = "[IMU]\tStatus: ";  message->concat(*status);
    message->concat(", Res: ");    message->concat(*result);
    message->concat(", Err: ");    message->concat(*error);
    message->concat(", Temp: ");   message->concat(*temp);
    message->concat("C\n");
}

// Get Radio status registers
void Platform::reportRadioStatus(int8_t *rssi)
{
    *rssi = (int8_t)rf69.rssiRead();
}

// Get IMU status registers
void Platform::reportIMUCalib(uint8_t *_system, uint8_t *acc, uint8_t *gyr, uint8_t *mag)
{
    bno.getCalibration(_system, gyr, acc, mag);
}

// Get IMU status registers
void Platform::reportIMUHealth(uint8_t *status, uint8_t *result, uint8_t *error, int8_t *temp)
{
    bno.getSystemStatus(status, result, error);
    *temp = (int8_t) bno.getTemp();
}

// Set status timeout
void Platform::setStatusTimeout()
{
    if (_cmd_body == _status_timeout) return;

    // Constrain
    if (_cmd_body < PLATFORM_STATUS_MIN_TIMEOUT)
        _cmd_body = PLATFORM_STATUS_MIN_TIMEOUT;
    if (_cmd_body > PLATFORM_STATUS_MAX_TIMEOUT)
        _cmd_body = PLATFORM_STATUS_MAX_TIMEOUT;
    
    // Assign new timeout
    _status_timeout = _cmd_body;

    // Prompt change
    String s0 = "!!\tStatus timeout changed (";
    s0.concat(_status_timeout);
    s0.concat(" sec)");
    transmit(s0);
    delay(2000);
}

// Set status format
void Platform::setStatusFormat()
{
    if (_cmd_opts == _status_report_format) return;

    // Constrain
    if (_cmd_opts < 0)
        _cmd_opts = 0;
    if (_cmd_opts > 1)
        _cmd_opts = 1;

    // Assign new format
    _status_report_format = _cmd_opts;

    // Prompt change
    String s1;
    switch (_status_report_format)
    {
        case 0: s1 = "verbose";
            break;
        case 1: s1 = "csv";
            break;
    }
    String s0 = "!!\tStatus report format changed (";
    s0.concat(s1);
    s0.concat(")");
    transmit(s0);
    delay(2000);

    // Header for CSV mode
    if (_cmd_opts == 1)
    {
        transmit("------------ CSV -----------");
        if (_HAS_IMU)
            transmit("t,rssi,calib,gyr,acc,mag,sys,res,err,T");
        if (!_HAS_IMU)
            transmit("t,rssi");
    }
}

// 4.2 DATA
// =====================================================================

// Set data type to transmit
void Platform::setDataType()
{
    if (_cmd_body == _data_type) return;

    // Prompt change
    String s0 = "!!\tData type changed (";
    s0.concat(_cmd_body);
    s0.concat(")");
    transmit(s0);
    delay(2000);

    // Assign change
    _data_type = _cmd_body;
}

// Get single report of IMU data
void Platform::reportData()
{
    updateDataRegisters();
    transmitData();
}

// Update stored IMU data
void Platform::updateDataRegisters()
{
    // Update timestamp
    time2string(&_epoch_str);
    switch(_data_type)
    {
        // ALL:     eul, quat, linacc, grav, acc, gyr, mag
        case 0:
            join(&_data_eu, bno.getVector(Adafruit_BNO055::VECTOR_EULER));
            join(&_data_qu, (imu::Quaternion)bno.getQuat());
            join(&_data_la, bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL));
            join(&_data_gv, bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY));
            join(&_data_ac, bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER));
            join(&_data_gy, bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE));
            join(&_data_mg, bno.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER));
            // String row[] = {t, eu, qu, la, gv, ac, gy, mg};
            // transmit(join(row));
            break;
        // RAW:     acc, gyr, mag
        case 1:
            join(&_data_ac, bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER));
            join(&_data_gy, bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE));
            join(&_data_mg, bno.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER));
            break;
        // FUSION:  eul, quat, linacc, grav
        case 2:
            join(&_data_eu, bno.getVector(Adafruit_BNO055::VECTOR_EULER));
            join(&_data_qu, (imu::Quaternion)bno.getQuat());
            join(&_data_la, bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL));
            join(&_data_gv, bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY));
            break;
        // COMPARE: eul, quat
        case 11:
            join(&_data_eu, bno.getVector(Adafruit_BNO055::VECTOR_EULER));
            join(&_data_qu, (imu::Quaternion)bno.getQuat());
            break;
        // COMPARE: linacc, grav
        case 12:
            join(&_data_la, bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL));
            join(&_data_gv, bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY));
            break;
        // COMPARE: linacc, grav, acc
        case 13:
            join(&_data_la, bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL));
            join(&_data_gv, bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY));
            join(&_data_ac, bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER));
            break;
        // 6DOF:    acc, gyr
        case 14:
            join(&_data_ac, bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER));
            join(&_data_gy, bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE));
            break;
        //  SINGLE: eul (deg)
        case 20:
            join(&_data_eu, bno.getVector(Adafruit_BNO055::VECTOR_EULER));
            break;
        //  SINGLE: quat
        case 21:
            join(&_data_qu, (imu::Quaternion)bno.getQuat());
            break;
        //  SINGLE: linacc
        case 22:
            join(&_data_la, bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL));
            break;
        //  SINGLE: grav
        case 23:
            join(&_data_gv, bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY));
            break;
        //  SINGLE: acc
        case 24:
            join(&_data_ac, bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER));
            break;
        //  SINGLE: gyr
        case 25:
            join(&_data_gy, bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE));
            break;
        //  SINGLE: mag
        case 26:
            join(&_data_mg, bno.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER));
            break;   
    }
}

// Assemble stored data and transmit
void Platform::transmitData()
{
    // Single magnitude
    if (_data_type >= 20 && _data_type < 30)
    {
        transmitSingleData();
        return;
    }

    // Multiple magnitude
    String row[8];
    if (_data_type == 0)
    {
        String row[] = {_epoch_str, _data_eu, _data_qu, _data_la, _data_gv,
                        _data_ac, _data_gy, _data_mg};
    }
    else if (_data_type == 1)
    {
        String row[] = {_epoch_str, _data_ac, _data_gy, _data_mg};
    }
    else if (_data_type == 2)
    {
        String row[] = {_epoch_str, _data_eu, _data_qu, _data_la, _data_gv};
    }
    else if (_data_type == 11)
    {
        String row[] = {_epoch_str, _data_eu, _data_qu};
    }
    else if (_data_type == 12)
    {
        String row[] = {_epoch_str, _data_la, _data_gv};
    }
    else if (_data_type == 13)
    {
        String row[] = {_epoch_str, _data_la, _data_gv, _data_ac};
    }
    else if (_data_type == 14)
    {
        String row[] = {_epoch_str, _data_ac, _data_gy};
    }
    join(&_data_row, row);
    transmit(_data_row);
}

// Transmit single-magnitude data
void Platform::transmitSingleData()
{
    _data_row = _epoch_str.concat(",");
    switch(_data_type)
    {
        //  SINGLE: euler
        case 20:    _data_row.concat(_data_eu);
            break;
        //  SINGLE: quat
        case 21:    _data_row.concat(_data_qu);
            break;
        //  SINGLE: linacc
        case 22:    _data_row.concat(_data_la);
            break;
        //  SINGLE: grav
        case 23:    _data_row.concat(_data_gv);
            break;
        //  SINGLE: acc
        case 24:    _data_row.concat(_data_ac);
            break;
        //  SINGLE: gyr
        case 25:    _data_row.concat(_data_gy);
            break;
        //  SINGLE: mag
        case 26:    _data_row.concat(_data_mg);
            break;
    }
    transmit(_data_row);
}


// 4.3 CALIB
// =====================================================================

// Calibrate IMU offsets
bool Platform::setCalibration(uint8_t offsets[22])
{
    // Default offsets: 3 acc, 3 gyro, 3 mag and 2 radius
    uint8_t _offsets[22] = {uint8_t lowByte(65522), uint8_t highByte(65522),
                            uint8_t lowByte(65499), uint8_t highByte(65499),
                            uint8_t lowByte(27), uint8_t highByte(27),

                            uint8_t lowByte(65534), uint8_t highByte(65534),
                            uint8_t lowByte(1), uint8_t highByte(1),
                            uint8_t lowByte(0), uint8_t highByte(0),

                            uint8_t lowByte(0), uint8_t highByte(0),
                            uint8_t lowByte(0), uint8_t highByte(65486),
                            uint8_t lowByte(0), uint8_t highByte(0),

                            uint8_t lowByte(1000), uint8_t highByte(1000),
                            uint8_t lowByte(1186), uint8_t highByte(1186)};
    
    if (!offsets)  offsets = _offsets;

    // Try setting offsets and check calibration twice
    for (uint8_t i = 0; i < 2; i++)
    {
        bno.setSensorOffsets(offsets);
        transmit("\tIMU calibration offsets set");
        delay(4000);

        // Check if IMU is calibrated
        if (bno.isFullyCalibrated())
        {
            transmit("\tIMU fully calibrated");
            return true;
        }
        if (i < 2) transmit("[WARN]\tIMU not calibrated. Retrying...");
    }

    transmit("[WARN]\tCalibration not achieved.");
    return false;
}

// Report calibration offsets
void Platform::reportCalibOffsets()
{
    char offsets[64];
    String s0, s1, s2;
    adafruit_bno055_offsets_t calibData;
    bno.getSensorOffsets(calibData);

    s0 = String(calibData.accel_offset_x, DEC);
    s1 = String(calibData.accel_offset_y, DEC);
    s2 = String(calibData.accel_offset_z, DEC);
    sprintf(offsets, "Accelerometer: X:%s, Y:%s, Z:%s", s0.c_str(), s1.c_str(), s2.c_str());
    transmit(offsets);

    s0 = String(calibData.gyro_offset_x, DEC);
    s1 = String(calibData.gyro_offset_y, DEC);
    s2 = String(calibData.gyro_offset_z, DEC);
    sprintf(offsets, "Gyroscope: X:%s, Y:%s, Z:%s", s0.c_str(), s1.c_str(), s2.c_str());
    transmit(offsets);

    s0 = String(calibData.mag_offset_x, DEC);
    s1 = String(calibData.mag_offset_y, DEC);
    s2 = String(calibData.mag_offset_z, DEC);
    sprintf(offsets, "Magnetometer: X:%s, Y:%s, Z:%s", s0.c_str(), s1.c_str(), s2.c_str());
    transmit(offsets);

    s0 = String(calibData.accel_radius, DEC);
    s1 = String(calibData.mag_radius, DEC);
    sprintf(offsets, "Accel Radius: %s, Mag Radius:%s", s0.c_str(), s1.c_str());
    transmit(offsets);
}

/**************************************************************************
 *  SET, TRANSMIT and LOG functions
 **************************************************************************/


// Send message by Radio and Serial if connected
void Platform::transmit(String message, bool newline)
{
    if (message.length() == 0) return;
    transmitRadio (message, newline);
    transmitSerial(message, newline);
}

// Send through serial USB (Debugging purposes)
void Platform::transmitSerial(String message, bool newline)
{
    if (!_HAS_SERIAL || !_SERIAL_BOOTED) return;
    if (!newline)
    {
        Serial.print(message);
        return;
    }
    Serial.println(message);
}

// Send by RF to Station (Real-life purposes)
void Platform::transmitRadio(String message, bool newline)
{
    // Escape if radio not booted yet
    if (_HAS_RADIO && !_RADIO_BOOTED)
    {
        transmitSerial("[!RF]\t", false);
        return;
    }

    char _len = message.length();
    char radiopacket[_len+1];
    message.toCharArray(radiopacket, _len+1);

    // Send a message to the DESTINATION!
    if (!rf69_manager.sendtoWait((uint8_t *)radiopacket, _len + 2, STATION_ADDRESS))
    {
        transmitSerial("[!SENT]\t", false);
        return;
    }

    // Notify ACK
    transmitSerial("[ACK]\t", false);
}

// Alert of error by blinking LED and prompting message
void Platform::alert(String message, uint8_t loops)
{
    if (message.length() > 0)
        transmitSerial(message);

    while (true)
    {
        for (uint8_t i = 0; i < loops; i++)
        {
            blink(LED, 100, false);
        }
        delay(1000 - 100 * 2 * loops);
    }
}

// Blink LED or reset device by pulse through RST PIN
void Platform::blink(uint8_t pin, uint8_t delta, bool inverted)
{
    if (inverted)
    {
        digitalWrite(pin, LOW);
        delay(delta);
        digitalWrite(pin, HIGH);
        delay(delta);
    }
    else
    {
        digitalWrite(pin, HIGH);
        delay(delta);
        digitalWrite(pin, LOW);
        delay(delta);
    }
}

/**************************************************************************
 *  AUX functions
 **************************************************************************/

// Join integers into single string
void Platform::join(String *message, int row[])
{
    for (uint8_t i = 0; i < sizeof(row); i++)
    {
        message->concat((int)row[i]);
        if (i < sizeof(row) - 1)
            message->concat(",");
    }
}

// Join strings into single string
void Platform::join(String *message, String row[])
{
    for (uint8_t i = 0; i < sizeof(row); i++)
    {
        message->concat((String)row[i]);
        if (i < sizeof(row) - 1)
            message->concat(",");
    }
}

// Join 3-coord vector into single string
void Platform::join(String *message, imu::Vector<3> vector)
{
    String sy, sz;
    *message = String(vector[0]);
    sy = String(vector[1]);
    sz = String(vector[2]);

    // Assemble s0,s1,s2(,)
    message->concat(","); message->concat(sy);
    message->concat(","); message->concat(sz);
}

// Join quaternion components into single string
void Platform::join(String *message, imu::Quaternion quat)
{
    String sx, sy, sz;
    *message = String(quat.w(), 4);
    sx = String(quat.x(), 4);
    sy = String(quat.y(), 4);
    sz = String(quat.z(), 4);

    // Assemble s0,s1,s2(,)
    message->concat(","); message->concat(sx);
    message->concat(","); message->concat(sy);
    message->concat(","); message->concat(sz);
}

// Parse single string into a 3-coord vector
bool Platform::string2vector(String *message, uint8_t *x, uint8_t *y, uint8_t *z)
{
    // Find positions of commas
    int8_t comma_i_xy = message->indexOf(',');
    int8_t comma_i_yz = message->lastIndexOf(',');
    if (comma_i_xy == -1 && comma_i_yz == -1)
        return false;

    // Split message "_,_,_"
    String sy = message->substring(comma_i_xy + 1, comma_i_yz);
    String sz = message->substring(comma_i_yz + 1);

    // Replace stranded commas just in case
    sy.replace(',', ' '); sy.trim();
    sz.replace(',', ' '); sz.trim();

    // Convert to integer and assign
    *x = message->toInt();
    *y = sy.toInt();
    *z = sz.toInt();
    return true;
}

// Get timestamp in milliseconds as a string
void Platform::time2string(String *s)
{
    *s = String(millis());
}
