/*  Platform library for the Air Bearing.
 *  Developed for IDR - UPM (2019).
 *  Author: David Criado Pernia
 * 
 *  Library that is used to operate the onboard
 *  platform subsystem, comprised by a Teensy 
 *  3.5 microcontroller, an Adafruit BNO055 
 *  inertial measurement unit and an Adafruit
 *  RFM69HCW transceiver.
 * 
 *  File contains:
 *  - Dependencies to external libraries
 *  - Platform, transceiver and IMU Constants
 *  - Inherited external constructors
 *  - Platform class header
 */

// Prevent double declaration
#ifndef __PLATFORM_H__
#define __PLATFORM_H__


/* 
 * Dependencies to external libraries.
 */

// Arduino
#include <SPI.h>                                 // Internal SPI Arduino library

// Third-party
#include <RH_RF69.h>                             // More info: https://goo.gl/ZeEy5k
#include <RHReliableDatagram.h>                  // More info: https://goo.gl/Ks9rfE
#include <i2c_t3.h>                              // More info: https://goo.gl/RyzFaM
#include <Adafruit_Sensor.h>                     // Unified Adafruit Sensor
#include <Adafruit_BNO055_t3.h>                  // More info: https://goo.gl/eAYKjn
#include <utility/imumaths.h>                    // Adds vector and quaternion namespaces


/* 
 * Platform, transceiver and IMU Constants
 */

// Operational modes
#define PLATFORM_OP_IDLE            (00)          //!< **Idle mode:** awaits for command
#define PLATFORM_OP_STATUS          (10)          //!< **Status mode:** awaits for command
#define PLATFORM_OP_DATA            (20)          //!< **Data mode:** prompts IMU data
#define PLATFORM_OP_CALIB           (30)          //!< **Calib mode:** calibrates IMU
/////////////////////////////////
// ADD HERE NEW OPMODE CONSTANTS

#define PLATFORM_OP_RATE            (10)          //!< Platform operations per second [Hz]

#define PLATFORM_IDLE_TIMEOUT       (30)          //!< **Interval** between idle reminders [sec]
#define PLATFORM_STATUS_MIN_TIMEOUT (1)           //!< Status reporting interval **upper** limit
#define PLATFORM_STATUS_MAX_TIMEOUT (10)          //!< Status reporting interval **lower** limit

// Interpret interactive commands
#define PLATFORM_CMD_INTERACTION    (60)          //!< Command **head** assigned to interactive reply
#define PLATFORM_CMD_YES            (1)           //!< Command **body** assigned to **Yes**
#define PLATFORM_CMD_NO             (2)           //!< Command **body** assigned to **No**

///////// Transceiver parameters

#define RF69_FREQ                   (433.0)       //!< Transceiver Frequency [Hz]

#define STATION_ADDRESS             (1)           //!< **Station** transceiver Address: where to send packets to
#define PLATFORM_ADDRESS            (2)           //!< **Platform** transceiver Address: platform's own adress

#define RFM69_RST                   (34)          //!< Teensy's pin to transceiver SPI **reset** pin
#define LED                         (13)          //!< Teensy's **LED** pin
#define RFM69_INT                   (33)          //!< Teensy's pin to transceiver SPI **interrupt** pin
#define RFM69_CS                    (15)          //!< Teensy's pin to transceiver SPI **chip select** pin
#define RFM69_SCK                   (14)          //!< Teensy's pin to transceiver SPI **clock** pin
#define RFM69_MOSI                  (28)          //!< Teensy's pin to transceiver SPI **MOSI** pin
#define RFM69_MISO                  (39)          //!< Teensy's pin to transceiver SPI **MISO** pin

///////// Inertial unit parameters

#define BNO_RST                     (23)          //!< Teensy's pin to inertial unit **reset** pin


//! Instance of the IMU driver
/*! Inherited external constructor.
 *  Solved thanks to: https://goo.gl/H4yL13
 * \param[in] iBus I2C Bus
 * \param[in] sensorID Sensor ID 
 * \param[in] aAddress Sensor I2C Address
 * \param[in] iMode I2C Mode (Master/Slave)
 * \param[in] pins I2C Pins
 * \param[in] pullup I2C Pull-ups (Internal/External)
 * \param[in] iRate I2C Rate [Hz]
 * \param[in] opeMode I2C Operational mode
 */
extern Adafruit_BNO055 bno;
//! Singleton instance of the radio driver
/*! Inherited external constructor.
 *  Solved thanks to: https://goo.gl/H4yL13
 * \param[in] slaveSelectPin the Arduino pin number of the output
 * to use to select the RF69 before accessing it. Defaults to SS.
 * \param[in] interruptPin Interrupt Pin number that is connected
 * to the RF69 DIO0 interrupt line. Defaults to 2.
 * \param[in] spi Pointer to the SPI interface object to use. 
 *  Defaults to the standard Arduino hardware SPI interface.
 */
extern RH_RF69 rf69;
//! Class to manage message delivery and receipt, using the driver above
/*! Inherited external constructor.
 *  Solved thanks to: https://goo.gl/H4yL13
 * \param[in] driver The RadioHead driver to use to transport messages.
 * \param[in] thisAddress The address to assign to this node. Defaults to 0
 */
extern RHReliableDatagram rf69_manager;


/*! \brief Platform class header.
 *  
 *  Used to operate the onboard
 *  platform subsystem, comprised by a Teensy 
 *  3.5 microcontroller, an Adafruit BNO055 
 *  inertial measurement unit and an Adafruit
 *  RFM69HCW transceiver.
 *
 *  Contains begin, telecommands, operation,
 *  and telemetry functionalities.
 */
class Platform
{
  public:     
    //! Class constructor
    /*! Parameters define alive devices. */
    Platform  (
                bool HAS_SERIAL = false,                //!< Serial USB coms enabled (Debugging)
                bool HAS_RADIO = true,                  //!< Transceiver enabled (RF)
                bool HAS_IMU = true                     //!< Inertial unit enabled (attitude)
              );

    /////////////////////////////////////////////////

    //! Begin all platform devices
    void begin                      (void);
    //! Begin Adafruit BNO055 inertial unit
    /*! 
     * Performs initial calibration too.
     * \return -1: not inited
     * \return 0: not calibrated
     * \return 1: success
     */
    int8_t beginIMU                 (void);
    //! Begin Adafruit RFM69HCW transceiver
    /*!
     *  Inits transceiver and sets frequency,
     *  transmitting power (dBm), and packets
     *  encryption key.
     *  \return true if inited successfully
     */
    bool beginRadio                 (void);
    //! Begin Serial USB interface
    /*! Allows sending telemetry and
     *  receiving telecommands through
     *  the serial port.
     */
    void beginSerial                (void);
    //! Configure Teensy board
    void beginTeensy                (void);

    /////////////////////////////////////////////////

    //! Listen for incoming commands
    void listen                     (void);
    //! Listen commands through Serial port
    /*! \return true if command arrived */
    bool listenSerial               (void);
    //! Listen commands through transceiver
    /*! \return true if command arrived */
    bool listenRadio                (void);
    //! Store incoming command in memory
    /*! 0,0,0 is not saved to prevent
     *  blank signals from being interpreted.
     *  \return true if command stored
     */
    bool storeCommand (
                        uint8_t* head,                    //!< Header defines operational mode or interaction
                        uint8_t* body,                    //!< Body contains first parameter
                        uint8_t* opts                     //!< Options contains second parameter
                      );
    //! Split incoming command into three numbers
    /*! Command "int,int,int" is saved as a triad of
     *  integers.
     *  \return true if command string split into integers
     */
    bool splitCommand (
                        uint8_t* buf,                     //!< Buffer stores incoming string with command
                        uint8_t* head,                    //!< Header defines operational mode
                        uint8_t* body,                    //!< Body contains first parameter
                        uint8_t* opts                     //!< Options contains second parameter
                      );

    /////////////////////////////////////////////////

    //! Interpret incoming commands
    void interpretCommand           (void);
    //! Interpret operational mode
    void interpretOpMode            (void);
    //! Interpret user interaction
    /*! A question is raised by the Platform to
     *  Station. Then, Platform waits for a reply
     *  to be sent as a triad of numbers.
     */
    void interpretInteraction       (void);
    //! Reset interaction
    /*! Await from Platform for a reply to a
     *  question is killed. Platform will no
     *  longer listen for an interaction response
     *  until a new question is raised.
     */
    void resetInteraction           (void);

    /////////////////////////////////////////////////

    //! Operate Platform subsystem
    void operate                    (void);
    //! Operate idle mode
    /*! Platform awaits for new command */
    void operateIdle                (void);
    //! Operate status mode
    /*! Platform reports system status */
    void operateStatus              (void);
    //! Operate data mode
    /*! Platform sends IMU attitude data */
    void operateData                (void);
    //! Operate calibration mode
    /*! Platform calibrates IMU */
    void operateCalib               (void);
    //! Change operational mode
    void setOperationalMode         (
                                      uint8_t op_mode       //!< Operational mode
                                    );

    /////////////////////////////////////////////////

    //! Report system status once
    void reportStatus               (void);
    //! Set interval between reports
    /*! Constrained between PLATFORM_STATUS_MIN_TIMEOUT
     *  and PLATFORM_STATUS_MAX_TIMEOUT.
     */
    void setStatusTimeout           (void);
    //! Set format of status reports
    /*! Allowed formats:
     *  0:  verbose
     *  1:  csv
     */
    void setStatusFormat            (void);
    //! Get transceiver status registers
    void reportRadioStatus          (
                                      int8_t *rssi          //!< Received Signal (-15 to -80)
                                    );
    //! Get inertial unit calibration registers
    void reportIMUCalib             (
                                      uint8_t *_system,     //!< Calibration overall status 0-3
                                      uint8_t *acc,         //!< Accelerometer calibration status (0-3)
                                      uint8_t *gyr,         //!< Gyroscope calibration status (0-3)
                                      uint8_t *mag          //!< Magnetometer calibration status (0-3)
                                    );
    //! Get inertial unit status registers and temperature
    /*!
      ** ## System Status (see section 4.3.58)
      *  - 0 = Idle
      *  - 1 = System Error
      *  - 2 = Initializing Peripherals
      *  - 3 = System Iniitalization
      *  - 4 = Executing Self-Test
      *  - 5 = Sensor fusio algorithm running
      *  - 6 = System running without fusion algorithms
      * 
      * ## Self Test Results
      *   1 = test passed, 0 = test failed
      *  - Bit 0 = Accelerometer self test
      *  - Bit 1 = Magnetometer self test
      *  - Bit 2 = Gyroscope self test
      *  - Bit 3 = MCU self test
      *  - 0x0F = all good!
      * 
      * ## System Error (see section 4.3.59)
      *  - 0 = No error
      *  - 1 = Peripheral initialization error
      *  - 2 = System initialization error
      *  - 3 = Self test result failed
      *  - 4 = Register map value out of range
      *  - 5 = Register map address out of range
      *  - 6 = Register map write error
      *  - 7 = BNO low power mode not available for selected operat ion mode
      *  - 8 = Accelerometer power mode not available
      *  - 9 = Fusion algorithm configuration error
      *  - A = Sensor configuration error
      * 
      */
    void reportIMUHealth            (
                                      uint8_t *status,      //!< Inertial unit overall health status
                                      uint8_t *result,      //!< Self Test Results status
                                      uint8_t *error,       //!< System error, if any
                                      int8_t *temp          //!< Temperature, in degC
                                    );
    //! Compose verbose status of transceiver status registers
    void verbalizeRadioStatus       (
                                      String *message,      //!< Composed message
                                      int8_t *rssi          //!< Received signal intensity (-15 to -80 dBm)
                                    );
    //! Compose verbose status of inertial unit calibration registers
    void verbalizeIMUCalib          (
                                      String *message,      //!< Composed message
                                      uint8_t *_system,     //!< Calibration overall status 0-3
                                      uint8_t *acc,         //!< Accelerometer calibration status (0-3)
                                      uint8_t *gyr,         //!< Gyroscope calibration status (0-3)
                                      uint8_t *mag          //!< Magnetometer calibration status (0-3)
                                    );
    //! Compose verbose status of inertial unit status registers and temperature
    void verbalizeIMUHealth         (
                                      String *message,      //!< Composed message
                                      uint8_t *status,      //!< Inertial unit overall health status
                                      uint8_t *result,      //!< Self Test Results status
                                      uint8_t *error,       //!< System error, if any
                                      int8_t *temp          //!< Temperature, in degC
                                    );

    /////////////////////////////////////////////////

    //! Set type of data to be retrieved
    /*! ## Allowed types
     * ### Very slow modes
     * 
     * - **0:** all data (eul, quat, linacc, grav, acc, gyr, mag)
     * - **1:** raw calibrated (acc, gyr, mag)
     * - **2:** fusion (eul, quat, linacc, grav)
     * 
     * ### Slow modes
     * 
     * - **11:** euler and quaternions
     * - **12:** linear acceleration and gravity
     * - **13:** linear acceleration, gravity, and accelerometer
     * - **14:** accelerometer and gyroscope
     * 
     * ### Fast modes
     * 
     * - **20:** Euler angles [degrees]
     * - **21:** quaternion [-]
     * - **22:** linear acceleration [m/s2]
     * - **23:** gravity vector [m/s2]
     * - **24:** accelerometer vector [m/s2]
     * - **25:** gyroscope vector [rad/s]
     * - **26:** magnetometer vector [uT]
     * 
     */
    void setDataType                (void);
    //! Update stored IMU data and send
    void reportData                 (void);
    //! Update stored IMU data
    void updateDataRegisters        (void);
    //! Assemble stored data and transmit
    void transmitData               (void);
    //! Transmit single-magnitude data
    void transmitSingleData         (void);

    /////////////////////////////////////////////////

    //! Calibrate IMU offsets
    /*! Set 11 calibration registers with new offsets.
     * Implemented second try if first is not successful.
     * Offsets correspond to:
     * - **1,2,3:** accelerometer (ax, ay, az)
     * - **4,5,6:** gyroscope (gx, gy, gz)
     * - **7,8,9:** magnetometer (mx, my, mz)
     * - **10:** accelerometer radius
     * - **11:** magnetometer radius
     * \return true if calibration is successful
     */
    bool setCalibration             (
                                      uint8_t offsets[22] = {} //!< Array of high-byte and low-byte offsets
                                    );
    //! Report stored calibration offsets
    void reportCalibOffsets         (void);

    /////////////////////////////////////////////////

    //! Operational mode
    uint8_t PLATFORM_OPERATION_MODE = PLATFORM_OP_IDLE;
    //! USB Serial baudrate
    const int32_t BAUDRATE = 115200;

  private:

    //! Send message by Radio and Serial if connected
    void transmit                   (
                                      String message = "", //!< Message to transmit
                                      bool newline = true  //!< Add newline character at the end
                                    );
    //! Send through serial USB (Debugging purposes)
    void transmitSerial             (
                                      String message = "", //!< Message to transmit via Serial USB to Debug computer
                                      bool newline = true  //!< Add newline character at the end
                                    );
    //! Send by RF to Station (Real-life purposes)
    void transmitRadio              (
                                      String message = "", //!< Message to transmit via RF to Station
                                      bool newline = true  //!< Add newline character at the end
                                    );

    /////////////////////////////////////////////////

    //! Alert of error by blinking LED and prompting message
    /*! ### LED blink error codes
     * - **Serial port failed**: Continuous blink (5 blinks per second) 
     * - **Radio begin failed**: 1 blink per second
     * - **IMU begin failed**: 3 blinks per second
     * - **DATA operational mode without IMU**: 2 blinks per second
     */
    void alert                      (
                                      String message = "", //!< Alert to transmit via Serial USB
                                      uint8_t loops = 1    //!< Loops that encode LED blinking
                                    );
    //! Blink LED or reset device by pulse through RST PIN
    void blink                      (
                                      uint8_t pin = LED,    //!< Target pin to trigger
                                      uint8_t delta = 10,   //!< Interval, in milliseconds
                                      bool inverted = false //!< Invert to first low and then high
                                    );

    /////////////////////////////////////////////////

    //! Join integers into single string
    /*! Function is duplicated to accept different argument types.
     *  More info: https://www.geeksforgeeks.org/polymorphism-in-c/
    */
    void join                       (
                                      String *message,      //!< Composed message
                                      int row[]             //!< Array of integers
                                    );
    //! Join strings into single string
    void join                       (
                                      String *message,      //!< Composed message
                                      String row[]          //!< Array of strings
                                    );
    //! Join 3-coord vector into single string
    void join                       (
                                      String *message,      //!< Composed message
                                      imu::Vector<3> vector //!< Input vector
                                    );
    //! Join quaternion components into single string
    void join                       (
                                      String *message,      //!< Composed message
                                      imu::Quaternion quat  //!< Input quaternion
                                    );
    //! Get timestamp in milliseconds as a string
    void time2string                (
                                      String *s             //!< Composed message
                                    );
    //! Parse single string into a 3-coord vector
    /*! \return true if string is parsed successfully */
    bool string2vector              (
                                      String *message,      //!< Composed message
                                      uint8_t *x,           //!< First coordinate
                                      uint8_t *y,           //!< Second coordinate 
                                      uint8_t *z            //!< Third coordinate
                                    );

    /////////////////////////////////////////////////

    bool      _HAS_SERIAL,                                  //!< Serial USB coms enabled (Debugging)
              _HAS_RADIO,                                   //!< Transceiver enabled (RF)
              _HAS_IMU;                                     //!< Inertial unit enabled (attitude)
    bool      _SERIAL_BOOTED              = false;          //!< Serial connection already inited
    bool      _RADIO_BOOTED               = false;          //!< Transceiver already inited
    bool      _IMU_BOOTED                 = false;          //!< Inertial unit already inited

    // Listen and interpret commands
    uint8_t   _cmd_head,                                    //!< Stores first parameter from incoming command 
              _cmd_body,                                    //!< Stores second parameter from incoming command 
              _cmd_opts;                                    //!< Stores third parameter from incoming command
    bool _new_command;                                      //!< Notifies change to active operational mode

    bool      _cmd_question_active        = false;          //!< Notifies when Platform asks a question to Station
    uint8_t   _cmd_interaction_response   = 0;              //!< Stores interactive reply from user

    // Operating commands
    uint64_t  _counter                    = 0;              //!< Sets transmit ratio

    // Status mode
    uint8_t   _status_timeout             = 5;              //!< Interval between reports
    uint8_t   _status_report_format       = 0;              //!< Report format (0: verbose, 1: csv)
    int8_t    _radio_rssi;                                  //!< Received signal intensity (-15 to -80 dBm)
    uint8_t   _imu_c_sys,                                   //!< Calibration overall status 0-3
              _imu_c_acc,                                   //!< Calibration accelerometer status 0-3 
              _imu_c_gyr,                                   //!< Calibration gyroscope status 0-3 
              _imu_c_mag;                                   //!< Calibration magnetometer status 0-3
    uint8_t   _imu_h_status,                                //!< Inertial unit overall health status
              _imu_h_res,                                   //!< Self Test Results status
              _imu_h_err;                                   //!< System error, if any
    int8_t    _imu_h_temp;                                  //!< Temperature, in degC
    String    _epoch_str,                                   //!< Timestamp in milliseconds
              _rad_str,                                     //!< Transceiver status
              _imu_c_str,                                   //!< Inertial unit calibration status
              _imu_h_str;                                   //!< Inertial unit health status

    // Data mode
    uint8_t   _data_type                  = 20;             //!< Specifies IMU data type
    uint8_t   _data_format                = 0;              //!< Unused parameter
    String    _data_row;                                    //!< Data row with IMU values
    String    _data_eu,                                     //!< Stores retrieved Euler angles from inertial unit
              _data_la,                                     //!< Stores retrieved linear acceleration vector from inertial unit
              _data_gv;                                     //!< Stores retrieved gravity vector from inertial unit
              _data_qu,                                     //!< Stores retrieved quaternion from inertial unit
    String    _data_ac,                                     //!< Stores retrieved accelerometer data from inertial unit
              _data_gy,                                     //!< Stores retrieved gyroscope data from inertial unit
              _data_mg;                                     //!< Stores retrieved magnetometer data from inertial unit

    // Calib mode
    bool      _calib_end                  = false;          //!< Notifies when calib operation ends

    // Transmit RF
    uint8_t   _radio_buffer[RH_RF69_MAX_MESSAGE_LEN];       //!< Prepares radio buffer to transmit
};
#endif
